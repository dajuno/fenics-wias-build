#!/bin/bash
source env_build.sh

MPICH_VERSION="3.2"

mkdir -p ${BUILD_DIR}/tar

echo "Downloading and building MPICH ${MPICH_VERSION}"

unset F90 F90FLAGS

cd ${BUILD_DIR} && \
   wget --quiet --read-timeout=10 -nc -P tar http://www.mpich.org/static/downloads/3.2/mpich-3.2.tar.gz && \
   tar -xzf tar/mpich-${MPICH_VERSION}.tar.gz && \
   cd mpich-${MPICH_VERSION} && \
   ./configure --prefix=${PREFIX} --enable-shared && \
   make -j ${BUILD_THREADS} && \
   make install

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi

