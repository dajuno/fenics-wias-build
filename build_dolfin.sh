#!/bin/bash
source env_build.sh

export PETSC_DIR=${PREFIX}
export SLEPC_DIR=${PREFIX}

mkdir -p $BUILD_DIR/tar

echo "Downloading and building dolfin ${FENICS_VERSION}"

cd $BUILD_DIR && \
   git clone https://bitbucket.org/fenics-project/dolfin.git && \
    cd dolfin && \
    git checkout ${FENICS_VERSION} && \
    mkdir -p build && \
    cd build && \
    pew in fenics-${TAG} cmake .. \
        -DCMAKE_INSTALL_PREFIX=${PREFIX} \
        -DDOLFIN_USE_PYTHON3=ON \
        -DPYTHON_EXECUTABLE:FILEPATH=$(pew in fenics-${TAG} which python${PYTHON_VERSION}) \
        -DCMAKE_BUILD_TYPE="Release" \
        -DCMAKE_CXX_COMPILER=mpicxx \
        -DCMAKE_CXX_FLAGS="-O3 -march=native -std=c++11" \
        -DCMAKE_C_COMPILER=mpicc \
        -DCMAKE_C_FLAGS="-O3 -march=native" \
        -DMPI_CXX_COMPILER=mpicxx \
        -DMPI_C_COMPILER=mpicc \
        -DDOLFIN_ENABLE_DOCS=OFF \
        -DCMAKE_VERBOSE_MAKEFILE=ON \
        -DDOLFIN_ENABLE_SPHINX=OFF \
        -DDOLFIN_ENABLE_UMFPACK=ON \
        -DDOLFIN_ENABLE_CHOLMOD=ON \
        -DDOLFIN_ENABLE_TRILINOS=OFF \
        -DDOLFIN_ENABLE_BENCHMARKS=ON \
        -DDOLFIN_ENABLE_VTK=OFF \
        -DDOLFIN_AUTO_DETECT_MPI=ON \
        -DBOOST_ROOT=${PREFIX} \
        -DBLAS_openblas_LIBRARY=${PREFIX}/lib/libopenblas.so \
        -DLAPACK_openblas_LIBRARY=${PREFIX}/lib/libopenblas.so \
        -DSLEPC_INCLUDE_DIRS=${PREFIX}/include \
        -DPETSC_INCLUDE_DIRS=${PREFIX}/include \
        -DSWIG_EXECUTABLE:FILEPATH=${PREFIX}/bin/swig \
        -DEIGEN3_INCLUDE_DIR:FILEPATH=${PREFIX}/include/eigen3 \
        -DZLIB_INCLUDE_DIR=${PREFIX}/include \
        -DZLIB_LIBRARY=${PREFIX}/lib/libz.so \
        -DPETSC4PY_INCLUDE_DIRS=$(pew in fenics-${TAG} pew sitepackages_dir)/petsc4py/include \
    2>&1 | tee cmake.log && \
    pew in fenics-${TAG} make -j ${BUILD_THREADS} && \
    pew in fenics-${TAG} make install

        # -DEIGEN3_INCLUDE_DIR=${EIGEN_DIR}/include \
        # -DHDF5_hdf5_LIBRARY_RELEASE=${HDF5_DIR}/lib/libhdf5.so \
        # -DMPIEXEC_MAX_NUMPROCS=120 \

        # -DHDF6_hdf5_LIBRARY_RELEASE=${HDF5_DIR}/lib/libhdf5.so \
        # -DCMAKE_Fortran_FLAGS="-O3 -xHost -ip -mkl" \
        # -DDOLFIN_ENABLE_PASTIX=OFF \
        # -DMPI_Fortran_COMPILER=${MPI_ROOT}/bin64/mpiifort \
        # -DDOLFIN_ENABLE_QT=OFF \
        # -DHDF5_C_COMPILER_EXECUTABLE=${HDF5_DIR}/bin/h5pcc \
        # -DZLIB_LIBRARY=${ZLIB_DIR}/lib/libz.a \
        # -DZLIB_INCLUDE_DIR=${ZLIB_DIR}/include \

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
