#!/bin/bash
source env_build.sh

mkdir -p ${BUILD_DIR}/tar

echo "Downloading and building Boost 1.65.1"

cd ${BUILD_DIR} && \
   wget --quiet --read-timeout=10 -nc https://dl.bintray.com/boostorg/release/1.65.1/source/boost_1_65_1.tar.gz -O tar/boost.tar.gz && \
   tar -xzf tar/boost.tar.gz && \
   cd boost_1_65_1 && \
   ./bootstrap.sh --prefix=$PREFIX --with-libraries=filesystem,program_options,random,regex,system,thread,timer,iostreams && \
   ./b2 -j ${BUILD_THREADS} install

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi



